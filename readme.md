﻿#wostore 4.1.0 -- 1008 接口文档

**目录**

[TOC]

##中转Host
http://xxx.xxx.xxx.xxx:8080/wostore/4101008
##中转服务器提交方式
GET

例如：
http://xxx.xxx.xxx.xxx:8080/wostore/4101008?reqtype=auto&modelNexus 5&userid=aaaaaaaaabbbbbbbbbbccc&versioncode=3&appid=xxxxxxxxxxxxxxxxxxxxxxxxxxxx&mac=aa:bb:cc:dd:ee&osversion=4.4.4&imei=cccccccccccccccc&appversion=1.3&game=游戏名称&ip=192.168.53.3&CPU_ABI=armeabi-v7a&CPU_ABI2=armeabi&appname=app名称&brand=google&imsi=bbbbbbbbbbbbbbbbbbbb&channelid=0001234&serial=adadadadadadaadadadad&appdeveloper=这里是公司的名字&cpid=9000241&payfee=1&serviceid=123123&feename=10个钻石&resolution=1024*768&vaccode=123125215290

注意：需要URL编码

##简化流程

发送到中转服务器

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| reqtype | auto | 使用简化流程 |
| model | Nexus 5 | Build.MODEL |
| userid | ---- | 获取方式见：参考代码 |
| versioncode | 3 | PackageInfo.versionCode |
| appid | ---- | ---- |
| mac | ---- | ---- |
| osversion | 4.4.4 | Build.VERSION.RELEASE |
| imei | ---- | ---- |
| appversion | ---- | 游戏版本	PackageInfo.versionName |
| game | ---- | 游戏名称 |
| ip | ---- | ---- |
| CPU_ABI | armeabi-v7a | Build.CPU_ABI |
| CPU_ABI2 | armeabi | Build.CPU_ABI2 |
| appname | ---- | APP 名称 |
| brand | google | Build.BRAND |
| imsi | ---- | ---- |
| channelid | ---- | ---- |
| serial | ---- | Build.SERIAL |
| appdeveloper | ---- | 这里是公司的名字 |
| cpid | ---- | ---- |
| payfee | 1 | 支付费用，单位（元） |
| serviceid | ---- | serviceid 就是 vac code |
| feename | 10个钻石 | 物品名称 |
| resolution | 1080\*1776 | 分辨率（\*号中间没有空格） |
| vaccode | ---- | ---- |
| orderid | ---- | 订单号长度为24位自定义字符 |
| cburl | http://xxxx.com/ | callback url |
| packagename | com.xxxx.balabala | 包名 | 

返回

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| content | ---- | 短信内容 |
| method | sendDataMessage | 短信发送方法，这里是 sendDataMessage，不是 sendTextMessage |
| number | 10655198018 | 目标号码 |
| port | 1 | 端口号 |

##手动流程
1.刚刚打开 app 发送 login 请求

2.支付前发送 precheck

3.发送验证码请求

4.发送扣费短信

5.短信发送后，发送 ordercheck 和 pay 日志

####HTTP 日志接口
- 登录
发送到中转服务器

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| reqtype | httpstate | 要求http 日志的返回 |
| step | login | 说明本次请求的返回类型是登录 |
| model | Nexus 5 | Build.MODEL |
| userid | ---- | 获取方式见：参考代码 |
| versioncode | ---- | PackageInfo.versionCode |
| appid | ---- | ---- |
| mac | ---- | ---- |
| osversion | ---- | Build.VERSION.RELEASE |
| packagename | com.xxxx.balabala | 包名 | 
| imei | ---- | ---- |
| appversion | ---- | 游戏版本	PackageInfo.versionName |
| game | ---- | 游戏名称 |
| ip | ---- | 本机IP |
| CPU_ABI | ---- | Build.CPU_ABI |
| CPU_ABI2 | ---- | Build.CPU_ABI2 |
| appname | ---- | APP 名称 |
| brand | ---- | Build.BRAND |
| imsi | ---- | ---- |
| channelid | ---- | ---- |
| serial | ---- | Build.SERIAL |

返回

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| url | ---- | 提交给官方服务器URL |
| method | post | 提交给官方服务器方式 |
| content | ---- | 加密后的内容 |

- precheck
发送到中转服务器

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| reqtype | httpstate | 要求http 日志的返回 |
| step | precheck | 说明本次请求的返回类型是 precheck |
| userid | ---- | 跟上面 userid 值相同 |
| mac | ---- | ---- |
| imei | ---- | ---- |
| imsi | ---- | ---- |
| appid | ---- | ---- |
| appname | ---- | 游戏名称 |
| appdeveloper | ---- | 这里是公司的名字 |
| channelid | ---- | ---- |
| cpid | ---- | ---- |
| payfee | 1 | 支付费用，单位（元） |
| serviceid | ---- | serviceid 就是 vac code |
| feename | 10个钻石 | 物品名称 |
| appversion | ---- | ---- |
| resolution | 1080\*1776 | 分辨率（\*号中间没有空格） |
| orderid | ---- | 订单号长度为24位自定义字符 |
| usercode | ---- | 用户手机号，跟发送验证码有关 |

返回

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| url | ---- | 提交给官方服务器URL |
| method | post | 提交给官方服务器方式 |
| content | ---- | 加密后的内容 |
| verify | ---- | 返回验证码相关的 json 串 |
| verifykey | ---- | 验证码解密key，同会话需要临时保留，接下来的提交需要用到 |

verify 说明

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| url | ---- | 提交给官方服务器URL |
| method | post | 提交给官方服务器方式 |
| content | ---- | 加密后的内容 |

- ordercheck
发送到中转服务器

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| reqtype | httpstate | 要求http 日志的返回 |
| step | ordercheck | 说明本次请求的返回类型是 ordercheck |
| imsi | ---- | ---- |
| vaccode | ---- | ---- |
| cpid | ---- | ---- |
| orderid | ---- | 订单号长度为24位自定义字符 |
| ordertime | ---- | 订单生成时间 |
| verifyret | ---- | 验证码请求发送给官方后的返回串 |
| verifykey | ---- | 验证码 key |

返回

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| url | ---- | 提交给官方服务器URL |
| method | post | 提交给官方服务器方式 |
| content | ---- | 加密后的内容 |

- pay
发送到中转服务器

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| reqtype | httpstate | 要求http 日志的返回 |
| step | pay | 说明本次请求的返回类型是 pay |
| imsi | ---- | ---- |
| imei | ---- | ---- |
| appid | ---- | ---- |
| payfee | 1.0 | 支付费用 |
| channelid | ---- | ---- |
| ordercheck | ---- | ordercheck 请求后，服务器的返回 |

返回

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| url | ---- | 提交给官方服务器URL |
| method | post | 提交给官方服务器方式 |
| content | ---- | 加密后的内容 |
| paystate | true | 返回当前日志支付结果的成功与否 |


#### 支付短信请求

- 支付短信
发送到中转服务器

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| reqtype | pay | 要求支付短信返回 |
| cpid | ---- | ---- |
| vaccode | ---- | ---- |
| channelid | ---- | ---- |
| imsi | ---- | ---- |
| orderid | ---- | 订单号长度为24位自定义字符 |
| verifyret | ---- | 验证码请求发送给官方后的返回串 |
| verifykey | ---- | 验证码 key |

返回

| 形参  | 参考值 | 说明 |
| ---- | ---- | ---- |
| content | ---- | 短信内容 |
| method | sendDataMessage | 短信发送方法，这里是 sendDataMessage，不是 sendTextMessage |
| number | 10655198018 | 目标号码 |
| port | 1 | 端口号 |
| ordertime | ---- | 返回订单时间，需要代入到 ordercheck 中 |