package consoleTester;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class UUIDCreater {
	public static String createUUID() {
		return getMD5(UUID.randomUUID().toString());
	}
	
	public static String getMD5(String src) {
        String v0_3;
        try {
            MessageDigest v0_1 = MessageDigest.getInstance("MD5");
            v0_1.update(src.getBytes());
            byte[] v2 = v0_1.digest();
            StringBuffer v3 = new StringBuffer("");
            int v1;
            for(v1 = 0; v1 < v2.length; ++v1) {
                int v0_2 = v2[v1];
                if(v0_2 < 0) {
                    v0_2 += 256;
                }

                if(v0_2 < 16) {
                    v3.append("0");
                }

                v3.append(Integer.toHexString(v0_2));
            }

            v0_3 = v3.toString();
        }
        catch(NoSuchAlgorithmException v0) {
            v0.printStackTrace();
            v0_3 = "";
        }

        return v0_3;
    }
}
